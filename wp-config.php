<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hoteles');

/** MySQL database username */
define('DB_USER', 'santiago');

/** MySQL database password */
define('DB_PASSWORD', 'a13santi');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'ui78v4^tP&ecDBKT>[i=Z[VzD~+k^@,8s(K+];Y|O9`:wJ00pD=/wJFy$kj1MfB&');
define('SECURE_AUTH_KEY', '$B5)HjaftWL Xge;`/yQ]n!-O*)3{LN-D$tU`Wo#m3~^*AfPOI6$+s=5Fc_q7I-T');
define('LOGGED_IN_KEY', 'UJbO`VCe1$giv|Kh#4mg,3]O^AvsZ8_Rt 9@<.NOx=57;uyn-ph1WZX;|^|qZ&dF');
define('NONCE_KEY', 'bky9;)$BjdX=]<|u)lvykoy%v<b;%[Iq9@vrt.:YfPqI_;vMb95vi!I,s[wbPmQ1');
define('AUTH_SALT', 'LP78;Q7WBu!`_sCKgIUB(q_bYS^oNl:4E(O|BH46oz)[jIjjBt#8Y&H{S,-b5m~@');
define('SECURE_AUTH_SALT', 'P/kn_.XjPE/S>r@ccxL<ao0Dyio|t9|w*{Y5R30|GP*!>$g8Q&iJL!_o&Ot$6L$x');
define('LOGGED_IN_SALT', 'R7X~NKC-w4pvx++e7k_|4JVQ<LcJf9f]]<NR|I&_]p{SLmzi>Y* Ozz$2/CtY2[g');
define('NONCE_SALT', 'tZ,kB$a![1kpz$?&`(+_knv6xyjUTz<z5].ztNd3i_F[RV:& Zy-#K+y|u![RdB7');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY',false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
