<?php


function valoracion_func($args,$content){
	if(current_user_can('administrator' )):
		error_log("Lo que sea");
	endif;
	for ($i=0; $i <(int)$args['valoracion'] ; $i++) { 
		echo '<i class="fa fa-star"></i>';
	}
}


add_shortcode( 'valoracion', 'valoracion_func' );



function sidebar_func() {
    register_sidebar( array(
        'name' => 'SideBar ficha-hotel' ,
        'id' => 'sidebar-1',
        'description' => 'Mostrar barra lateral',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget'  => '</li>',
				'before_title'  => '<h2 class="widgettitle">',
				'after_title'   => '</h2>',
    ) );
}

add_action( 'widgets_init', 'sidebar_func' );


function remove_some_widgets(){

	// Unregister some of the TwentyTen sidebars
	unregister_sidebar( 'sidebar' );

}
add_action( 'widgets_init', 'remove_some_widgets', 11 );


//filtros
function mifuncion_func($precio){
	$precio .= "€";
	return $precio;
}

add_filter( 'mi_filtro', 'mifuncion_func', 1, 1 );


function post_published_notification( $ID, $post ) {
    $author = $post->post_author; /* Post author ID. */
    $name = get_the_author_meta( 'display_name', $author );
    $email = get_the_author_meta( 'user_email', $author );
    $title = $post->post_title;
    $permalink = get_permalink( $ID );
    $edit = get_edit_post_link( $ID, '' );
    $to = "yo@santipm.com";
    $subject = sprintf( 'Published: %s', $title );
    $message = sprintf ('Congratulations, %s! Your article “%s” has been published.' . "\n\n", $name, $title );
    $message .= sprintf( 'View: %s', $permalink );
    $headers[] = '';
    wp_mail( $to, $subject, $message, $headers );
}
add_action( 'publish_post', 'post_published_notification', 10, 2 );


function mi_funcion_action($query){
	global $pagename;
	echo $pagename;
	$query->set( 'posts_per_archive_page', 2 );
}


add_action('pre_get_posts','mi_funcion_action' );
