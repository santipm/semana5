<?php
/*
Template Name: Listado de Hoteles
*/
?>

<?php $mts_options = get_option('splash'); ?>
<?php get_header(); ?>
<div id="page">
	<div class="<?php mts_article_class(); ?>">
		<div id="content_box">
			<h1 class="postsby">
				<?php if (is_category()) { ?>
					<span><?php single_cat_title(); ?><?php _e(" Archive", "mythemeshop"); ?></span>
				<?php } elseif (is_tag()) { ?> 
					<span><?php single_tag_title(); ?><?php _e(" Archive", "mythemeshop"); ?></span>
				<?php } elseif (is_author()) { ?>
					<span><?php  $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); echo $curauth->nickname; _e(" Archive", "mythemeshop"); ?></span> 
				<?php } elseif (is_day()) { ?>
					<span><?php _e("Daily Archive:", "mythemeshop"); ?></span> <?php the_time('l, F j, Y'); ?>
				<?php } elseif (is_month()) { ?>
					<span><?php _e("Monthly Archive:", "mythemeshop"); ?>:</span> <?php the_time('F Y'); ?>
				<?php } elseif (is_year()) { ?>
					<span><?php _e("Yearly Archive:", "mythemeshop"); ?>:</span> <?php the_time('Y'); ?>
				<?php } ?>
			</h1>
	
			
			<?php 
				$taxonomia = (empty($_GET['cat'])) ? 'all' : $_GET['cat'];
				$args= array(
								'post_type'		=> 'hotel',
								'tax_query'	=> array(

														array(
															'taxonomy'		=> 'estrellas',
															'field'				=> 'slug',
															'terms'				=> $taxonomia,
														),
															),
							);
				$hoteles = new WP_Query($args);
			?>		
			<?php $j = 0; if ($hoteles->have_posts()) : while ($hoteles->have_posts()) : $hoteles->the_post(); ?>
				<article class="latestPost excerpt  <?php echo (++$j % 3 == 0) ? 'last' : ''; if ($selected_layout == 'grid') echo ' grid'; ?>">
					<header>
						<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="nofollow" id="featured-thumbnail">
								<?php echo '<div class="featured-thumbnail">'; the_post_thumbnail('featured',array('title' => '')); echo '</div>'; ?>
							<?php if(get_post_meta($post->ID, 'mts_overall_score', true)): ?>
								<span class="rating"><img src="<?php bloginfo('template_directory'); ?>/images/stars/<?php echo get_post_meta($post->ID, 'mts_overall_score', true); ?>.png"/></span>
							<?php elseif (function_exists('wp_review_show_total')) :
								wp_review_show_total(true, 'rating');
							endif; ?>
						</a>
						<h2 class="title front-view-title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
						<?php if($mts_options['mts_home_headline_meta'] == '1') { ?>
							<div class="post-info">
								<span class="theauthor"><i class="fa fa-money"></i> <?php //echo apply_filters('mi_filtro',get_post_meta( get_the_id(), 'campos_hotel_precio' , true ); ?></span>  
								<span class="thetime"><i class="fa fa-star"></i> <?php echo get_post_meta( get_the_id(), 'campos_hotel_valoracin' , true ); ?></span>  
								<span class="thecategory"><i class="fa fa-phone"></i> <?php echo get_post_meta( get_the_id(), 'campos_hotel_telfono' , true ); ?></span> 
							</div>
						<?php } ?>
					</header>
					<div class="front-view-content">
						<?php echo mts_excerpt(48);?>
					</div>
				</article><!--.post excerpt-->
			<?php endwhile; else:
											echo "No se";
											endif; ?>
			<!--Start Pagination-->
			<?php if ($mts_options['mts_pagenavigation'] == '1' ) { ?>
				<?php  $additional_loop = 0; mts_pagination($additional_loop['max_num_pages']); ?>           
			<?php } else { ?>
				<div class="pagination">
					<ul>
						<li class="nav-previous"><?php next_posts_link( '<i class="fa fa-angle-left"></i> '. __( 'Previous', 'mythemeshop' ) ); ?></li>
						<li class="nav-next"><?php previous_posts_link( __( 'Next', 'mythemeshop' ).' <i class="fa fa-angle-right"></i>' ); ?></li>
					</ul>
				</div>
			<?php } ?>
			<!--End Pagination-->
		</div>
	</div>
	<div class="sidebar">
		<?php get_sidebar('etiquetas'); ?>
		<?php if(is_active_sidebar('sidebar-1' )):
						dynamic_sidebar('sidebar-1' );
					else:
						echo "No está";
					endif;
		 ?>
	</div>

<?php get_footer(); ?>